
public class Konsolenausgabe {

	public static void main(String[] args) {
		
		System.out.print("Hallo Welt\n");
		
		System.out.println("Er sagte: \"Guten Tag!\"");	
		
		int alter = 17;
		String name = "Niklas";
		System.out.print("Ich hei�e " + name + " und bin " + alter + " Jahre alt.");
		
		System.out.printf("Hello %s!%n", "World");
		
		String s = "Kein-JAVA";
		System.out.printf( "\n|%s|\n", s );
	
		System.out.printf( "|%20s|\n", s ); 
		
		System.out.printf( "|%-20s|\n", s ); 
		
		System.out.printf( "|%5s|\n", s ); 
		
		System.out.printf( "|%.4s|\n", s ); 
		
		System.out.printf( "|%20.4s|\n", s ); 
		
		int i = 123;
		
		System.out.printf( "|%d| |%d|\n" , i, -i); // |123| |-123|
		
		System.out.printf( "|%5d| |%5d|\n" , i, -i); // | 123| | -123|
		
		System.out.printf( "|%-5d| |%-5d|\n" , i, -i); // |123 | |-123 |
		
		System.out.printf( "|%+-5d| |%+-5d|\n" , i, -i); // |+123 | |-123 |
		
		System.out.printf( "|%05d| |%05d|\n\n", i, -i); // |00123| |-0123|
		
		System.out.printf( "|%X| |%x|\n", 0xabc, 0xabc ); // |ABC| |abc|
	}

}
