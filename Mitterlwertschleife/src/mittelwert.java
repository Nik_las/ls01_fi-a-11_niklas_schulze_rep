import java.util.Scanner;

public class mittelwert{

	public static void main(String[] args) {
		double zahl, summe = 0;
		double m;
		Scanner myScanner = new Scanner(System.in);
		System.out.println("Dieses Programm berechnet den Mittelwert  Zahlen.");
		System.out.println("Wie viele zahlen wollen sie eingeben?");
		int anzahl = myScanner.nextInt();

		for (int i = 0; i < anzahl; i++) {
			summe = eingabe(myScanner, "Bitte Zahl eingeben:")	;
			summe = summe + zahl;
		}

		m = mittelwertBerechnung(summe, anzahl);

		ausgabe(m);

		myScanner.close();
	}

	public static double eingabe(Scanner ms, String text) {
		System.out.print(text);
		double zahl = ms.nextDouble();
		return zahl;
	}

	public static double mittelwertBerechnung(double zahl1, double zahl2) {
		double m = (zahl1 + zahl2) / 2.0;
		return m;
	}

	public static void ausgabe(double mittelwert) {
		System.out.println("Der errechnete Mittelwert beider Zahlen ist: " + mittelwert);
	}
}